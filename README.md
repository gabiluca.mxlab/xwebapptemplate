# ASP NetCore 2.1 Missing Template Futures!

Adds to default MS WebApp template:
- Autovalidate CSRF token
- Localization (cookie based on views, controllers, models data adnotations) + UI switch, no RazorPages, works with standard Identity (MVC)
- MVC Areas
- MySql database as default
- Google auth