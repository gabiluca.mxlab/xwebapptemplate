﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using X.Data;
using X.Hubs;
using X.Models;

namespace X
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Localization
            services.AddLocalization(o =>
            {
                // We will put our translations in a folder called Resources
                o.ResourcesPath = "Resources";
            });
            #endregion

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseMySql(Configuration["DefaultConnection"]));

            services.AddDefaultIdentity<ApplicationUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            #region Google Auth
            services.AddAuthentication().AddGoogle(googleOptions =>
                {
                    googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                    googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                });

            #endregion
            services.AddAuthorization();

            #region SignalR
            services.AddSignalR();
            #endregion

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            })
            .AddDataAnnotationsLocalization()
            .AddViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.Suffix)
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            #region Localization
            IList<CultureInfo> supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("en"),
                new CultureInfo("ro"),
            };

            var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();

            localizationOptions.Value.DefaultRequestCulture = new RequestCulture("ro");
            localizationOptions.Value.SupportedCultures = supportedCultures;
            localizationOptions.Value.SupportedUICultures = supportedCultures;

            //with query string localization
            //var requestProvider = new RouteDataRequestCultureProvider();


            var acceptProvider = localizationOptions.Value.RequestCultureProviders
                                    .OfType<AcceptLanguageHeaderRequestCultureProvider>()
                                    .First();

            localizationOptions.Value.RequestCultureProviders.Remove(acceptProvider);

            var cookieProvider = localizationOptions.Value.RequestCultureProviders
                                    .OfType<CookieRequestCultureProvider>()
                                    .First();

            // Set the new cookie name
            cookieProvider.CookieName = "UserCulture";
            app.UseRequestLocalization(localizationOptions.Value);
            #endregion

            #region Localization
            //with cookie localization
            app.UseMvc(mvcRoutes =>
            {
                //use areas - folder structure is required Admin
                mvcRoutes.MapRoute("admin-area", "{area:exists}/{controller=Admin}/{action=Index}/{id?}");
                //use areas - Internal
                mvcRoutes.MapRoute("internal-area", "{area:exists}/{controller=Internal}/{action=Index}/{id?}");

                mvcRoutes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //with query string localization
            //app.UseRouter(routes =>
            //    {
            //        routes.MapMiddlewareRoute("{culture=en}/{*mvcRoute}", subApp =>
            //        {
            //            subApp.UseRequestLocalization(localizationOptions);

            //            subApp.UseMvc(mvcRoutes =>
            //            {
            //            //use areas - folder structure is required Admin
            //            mvcRoutes.MapRoute("admin-area", "{area:exists}/{controller=Admin}/{action=Index}/{id?}");
            //            //use areas - Internal
            //            mvcRoutes.MapRoute("internal-area", "{area:exists}/{controller=Internal}/{action=Index}/{id?}");

            //                mvcRoutes.MapRoute(
            //                    name: "default",
            //                    template: "{culture=en}/{controller=Home}/{action=Index}/{id?}");
            //            });
            //        });
            //    }); 
            #endregion

            #region SignalR
            app.UseSignalR(routes =>
            {
                routes.MapHub<Notifications>("/notifications");
            });
            #endregion
        }
    }
}
