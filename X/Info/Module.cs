﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X.Info
{
    enum UISlotType
    {
        MenuItem,
        Widget,
        Notification,
        RTNotification,
        TimelineActivity
    }
    class ModuleItem
    {


        public Register()
        {
            //seed data
            //register crud actions
        }
        public UnRegister()
        {
            //reverse Register
        }

        public EnableUI()
        {
            //add item to BO UI

        }

        public DisableUI()
        {
            //rem item to BO UI

        }

        public GetParentModule()
        {

        }

        public GetUISlotType()
        {

        }
    }

    class Module
    {
        //moduleItems:ModuleItemAction[];

    public Register(path, icon, pos)
        {
            this.moduleItems.forEach(it => {
                it.Register();
            });
        }
        public UnRegister()
        {
            //reverse Register
        }

        public RegisterAction()
        {
            //seed data
            //register crud actions
        }
        public UnRegisterAction()
        {
            //reverse Register
        }
    }

}
