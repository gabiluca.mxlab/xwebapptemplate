﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace X.Controllers
{
    public class LanguageController : Controller
    {

        public IActionResult Index(string returnUrl, string cultureName="ro")
        {
            Response.Cookies.Append(
                 "UserCulture",
                 CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(cultureName)),
                 new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) });
            return Redirect(returnUrl);
        }
    }
}