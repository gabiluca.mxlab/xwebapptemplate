﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X.Hubs
{
    public class Notifications: Hub
    {

        public Task Send(string jssMessage)
        {
            var sender = Context.User?.Identity?.Name ?? "anonymus";
            return Clients.All.SendAsync("ClientMessageHandler", sender, jssMessage);
        }
    }
}
